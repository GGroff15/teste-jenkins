package teste.jenkins;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;

@SpringBootTest
@Profile("smoke-test")
class TesteJenkinsApplicationTests {

	@Test
	void contextLoads() {
	}

}
