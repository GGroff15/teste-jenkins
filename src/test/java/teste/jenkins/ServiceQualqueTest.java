package teste.jenkins;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringJUnitConfig(ServiceQualquer.class)
@Profile("integration-test")
class ServiceQualqueTest {
	
	@Autowired
	private ServiceQualquer qualquer;

	@Test
	void testSomar() {
		int resultado = qualquer.somar(1, 2);
		assertEquals(3, resultado, "Falha na soma dos numero 1 e 2");
	}

}
