FROM 17-jdk-slim
ADD /target/${JAR_FILE} /app/TesteJenkins.jar
EXPOSE 8180
CMD ["java", "-jar", "/app*TesteJenkins.jar"]
